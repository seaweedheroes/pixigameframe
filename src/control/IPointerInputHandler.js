/*
 * @Author: Hendranus Vermeulen 
 * @Date: 2020-01-25 19:38:31 
 * @Last Modified by: Hendranus Vermeulen
 * @Last Modified time: 2020-01-27 10:20:57
 * 
 * Interface IPointerInputHandler for controller classes to implement
 * 
 */

const IPointerInputHandler = superclass => class extends superclass {
    constructor() {
        super();
        // Check Interface contract is fullfilled
        if (    this.pointerdownHandler == null ) 
        {
            // Error Type 3. The child has not implemented this method.
            throw new TypeError("IPointerInputHandler: Fullfill contract, implement all methods.");
        }
    }
};
export { IPointerInputHandler }