/*
 * @Author: Hendranus Vermeulen 
 * @Date: 2020-01-27 09:41:20 
 * @Last Modified by: Hendranus Vermeulen
 * @Last Modified time: 2020-01-27 10:20:46
 * 
 * Interface IGameStateHandler for controller classes to implement
 * 
 */
const IGameStateHandler = superclass => class extends superclass {
    constructor() {
        super();
        // Check Interface contract is fullfilled
        if ( this.stateChangeHandler == null ) 
        {
            // Error Type 3. The child has not implemented this method.
            throw new TypeError("IGameStateHandler: Fullfill contract, implement all methods.");
        }
    }
};
export { IGameStateHandler }