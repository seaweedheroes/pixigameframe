/*
 * @Author: Hendranus Vermeulen 
 * @Date: 2020-01-25 19:38:37 
 * @Last Modified by: Hendranus Vermeulen
 * @Last Modified time: 2020-01-27 10:41:25
 * 
 * Class GameController implements IGameStateHandler and IPointerInputHandler interfaces to catch events delegated by views, 
 * it provides controller functionality and update models of the MVC pattern
 * 
 */
import { IPointerInputHandler } from "./IPointerInputHandler";
import { IGameStateHandler } from "./IGameStateHandler";

class GameController extends IGameStateHandler( IPointerInputHandler( Object ) ) { // Using class composition to implement multiple Interfaces
    /**
     * Constructor
     * @param	model	Model
     */
    constructor(model) {
        super();
        this.model = model;
    }
    /**
     * Catch state change event delegated from view
     * @param	data
     */
    stateChangeHandler(data){
        this.model.setState(data);
    }
    /**
     * Catch pointerdown event delegated from view
     * @param	data
     */
    pointerdownHandler(data){
        this.model.setKey(data.key);
    }
}
export { GameController }