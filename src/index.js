/*
 * @Author: Hendranus Vermeulen 
 * @Date: 2020-01-25 19:36:57 
 * @Last Modified by: Hendranus Vermeulen
 * @Last Modified time: 2020-01-27 10:51:01
 */

const PIXI = require('pixi.js');
window.PIXI = PIXI;

const app = new PIXI.Application();
document.body.appendChild(app.view);

import { Model } from './model/Model.js';
import { GameController } from './control/GameController.js';
import { RootScreen } from './view/component/RootScreen.js';

const model = new Model();
const controler = new GameController(model);
const rootScreen = new RootScreen(model, controler);


app.stage.addChild(rootScreen);
model.setKey(0);
