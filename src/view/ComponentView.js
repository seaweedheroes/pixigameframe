/*
 * @Author: Hendranus Vermeulen 
 * @Date: 2020-01-26 10:40:40 
 * @Last Modified by: Hendranus Vermeulen
 * @Last Modified time: 2020-02-19 23:15:47
 * 
 * Class ComponentView is the abstract class of component Views, it listens for events and delegates these events to the controller
 * DO NOT IMPLEMENT ABSTRACT CLASS
 * 
 */
import { Container } from '@pixi/display';
import { PGFEvent } from '../events/PGFEvent.js';

class ComponentView extends Container {
    /**
     * Constructor 
     * @param	model	Model of component
     * @param	controller	Controller of component
     */
    constructor(model, controller) {
        super();
        this.model = model;
        this.controller = controller;
        if (this.constructor === ComponentView) {
            // Error Type 1. Abstract class can not be constructed.
            throw new TypeError("ComponentView: Constructing abstract class.");
        }
        //else (called from child)
        // Check if all instance methods are implemented.
        if (this.update === ComponentView.prototype.update) {
            // Error Type 4. Child has not implemented this abstract method.
            throw new TypeError("ComponentView: update needs to be implemented");
        }
        // Catch update event 
        this.model.on(PGFEvent.CHANGE, (obj) => 
            this.update(obj.key)
        );
    }
    // A static abstract method.
    static update() {
        if (this === ComponentView) {
            // Error Type 2. Abstract methods can not be called directly.
            throw new TypeError("ComponentView: Can not call static abstract method.");
        } else if (this.update === ComponentView.update) {
            // Error Type 3. The child has not implemented this method.
            throw new TypeError("ComponentView: Please implement update.");
        } else {
            // Error Type 5. The child has implemented this method but also called `super.foo()`.
            throw new TypeError("ComponentView: Do not call static abstract method from child.");
        }
    }
    /**
     * ABSTRACT Method (must be overridden in a subclass)
     * Catch update event 
     */
    update() {
        throw new TypeError("ComponentView: Do not call abstract method from child.");
    }
    /**
     * Add component to component, not supported for component itself
     * @param   component
     */
    add(component){
        throw new TypeError(("ComponentView: add operation not supported."));
    }
    /**
     * Remove component, not supported for component itself
     * @param	component
     */
    remove(component){
        throw new TypeError(("ComponentView: remove operation not supported."));
    }
    /**
     * Get child component, not supported for component itself
     * @param	index Index
     * @return
     */
    getChild(index){
        throw new TypeError(("ComponentView: getChild operation not supported."));
    }
}
export { ComponentView }