/*
 * @Author: Hendranus Vermeulen 
 * @Date: 2020-01-26 14:01:41 
 * @Last Modified by: Hendranus Vermeulen
 * @Last Modified time: 2020-01-27 09:53:11
 * 
 * Class LeafView
 * USE THIS FOR VIEWS THAT CONTAIN NO OTHER VIEWS
 * 
 */

class LeafView {
    constructor(model, controller) {
        super(model, controller);
    }
    update(){
        console.log(`Leaf shows Key: ${this.model.data.key}`);
    }
}
export {LeafView}