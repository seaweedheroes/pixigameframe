/*
 * @Author: Hendranus Vermeulen 
 * @Date: 2020-01-26 10:40:46 
 * @Last Modified by: Hendranus Vermeulen
 * @Last Modified time: 2020-01-27 10:43:08
 * 
 * Class CompositeView is the abstract class of composite Views, it inherits ComponentView and extends it to make it compositable
 * DO NOT IMPLEMENT ABSTRACT CLASS
 * 
 */
import { ComponentView } from './ComponentView.js';

class CompositeView extends ComponentView {
    /**
     * Constructor 
     * @param	model	Model of component
     * @param	controller	Controller of component
     */
    constructor(model, controller) {
        super(model, controller);
        if (this.constructor === CompositeView) {
            // Error Type 1. Abstract class can not be constructed.
            throw new TypeError("CompositeView: Constructing abstract class.");
        }
        this.children = [];
    }
    /**
     * Catch update event and call update for every component
     */
    update() {
        for (child in this.children) {
            child.update();
        }
    }
    /**
     * Add component to component, overriden and supported
     * @param	component
     */
    add(component){
        this.children.push(component);
        this.addChild(component);
    }
    /**
     * Remove component, overriden and supported
     * @param	component
     */
    remove(component){
        const index = this.children.indexOf(component);
        if (index > -1) {
            this.children.splice(index, 1);
            this.removeChild(component);
        }
    }
    /**
     * Get child component, overriden and supported
     * @param	index Index
     * @return
	*/
    getChild(index){
        return this.children[index];
    }
}
export { CompositeView }