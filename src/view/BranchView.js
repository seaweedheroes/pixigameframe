/*
 * @Author: Hendranus Vermeulen 
 * @Date: 2020-01-26 14:01:49 
 * @Last Modified by: Hendranus Vermeulen
 * @Last Modified time: 2020-01-27 09:52:11
 * 
 * Class BranchView is the concrete implementation of the composite View
 * USE THIS FOR VIEWS THAT CONTAIN OTHER VIEWS OR DISPLAY OBJECTS
 * 
 */
import { CompositeView } from "./CompositeView.js";

class BranchView extends CompositeView {
    constructor(model, controller) {
        super(model, controller);
    }
    update(){
        console.log(`Branch shows Key: ${this.model.data.key}`);
    }
}
export {BranchView}