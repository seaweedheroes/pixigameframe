/*
 * @Author: Hendranus Vermeulen 
 * @Date: 2020-01-26 14:01:33 
 * @Last Modified by: Hendranus Vermeulen
 * @Last Modified time: 2020-01-27 10:04:07
 * 
 * Class RootView is the root node and view for the concrete implementation of the composite View
 * IDEALLY YOU SHOULD ONLY IMPLEMENT ONE ROOT VIEW
 * 
 */
import { CompositeView } from "./CompositeView.js";

class RootView extends CompositeView {
    constructor(model, controller) {
        super(model, controller);
    }
}
export {RootView}