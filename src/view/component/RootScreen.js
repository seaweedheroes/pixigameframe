/*
 * @Author: Hendranus Vermeulen 
 * @Date: 2020-01-27 10:45:15 
 * @Last Modified by: Hendranus Vermeulen
 * @Last Modified time: 2020-02-20 09:02:02
 * 
 * Class RootScreen is a concrete implementation from RootView, to keep RootView abstracted from this specific implementation
 * IDEALLY YOU SHOULD ONLY IMPLEMENT ONE ROOT SCREEN
 * 
 */
import { RootView } from "../RootView.js";
import { Screen } from "./Screen.js"
import { GameStates } from "../../model/GameStates.js";

class RootScreen extends RootView{
    constructor(model, controller) {
        super(model, controller);
        var screen = new Screen(model, controller);
        this.add(screen);
    }
    update(){
        console.log(`RootScreen shows Key: ${this.model.data.key}`);
        console.log(`RootScreen shows CurrentState: ${this.model.data.currentState}`);
        switch(this.model.data.currentState){
            // Check state and update views
            // Set state in model to Loading! Currently, button press changes preloader progress status :(
            // TODO Implement media LoadController and emmits Game State changed events when loads complete
            case GameStates.NOT_LOADED : console.log("Show Preloader!"); break;
            case GameStates.LOADED : console.log("Show game!"); break;
        }
    }
}
export {RootScreen}