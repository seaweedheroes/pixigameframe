/*
 * @Author: Hendranus Vermeulen 
 * @Date: 2020-01-26 17:54:36 
 * @Last Modified by: Hendranus Vermeulen
 * @Last Modified time: 2020-02-19 22:49:02
 * 
 * Class Button is very basic button to test firing pointer event from nested view
 * 
 */

import { PGFPointerEvent } from "../../events/PGFPointerEvent.js";
import { Container } from "pixi.js";
import { GameStates } from "../../model/GameStates.js";

class Button extends Container {
    constructor(controller) {
        super();
        this.interactive = true;
        this.buttonMode = true;
        this.controller = controller;
        let counter = 0;
        const graphics = new PIXI.Graphics();
        // Rectangle
        graphics.beginFill(0xDE3249);
        graphics.drawRect(50, 50, 100, 100);
        graphics.endFill();
        this.addChild(graphics);

        if(this.controller){
            this.on(PGFPointerEvent.POINTER_DOWN, () => {
                this.controller.stateChangeHandler(GameStates.LOADED);
                this.controller.pointerdownHandler({ key : ++counter }) //TODO Need to send component name and event details in object
            })
        }
    }
}
export {Button}