/*
 * @Author: Hendranus Vermeulen 
 * @Date: 2020-01-26 18:11:33 
 * @Last Modified by: Hendranus Vermeulen
 * @Last Modified time: 2020-01-27 10:49:07
 * 
 * Class Screen is a concrete implementation from BranchView, to keep BranchView abstracted from this specific implementation
 * 
 */
import { BranchView } from "../BranchView.js";
import { Button } from "./Button.js";

class Screen extends BranchView{
    constructor(model, controller) {
        super(model, controller);
        var button = new Button(controller);
        this.add(button);
    }
    update(){
        console.log(`Screen shows Key: ${this.model.data.key}`);
    }
}
export {Screen}