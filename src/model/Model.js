/*
 * @Author: Hendranus Vermeulen 
 * @Date: 2020-01-25 19:38:06 
 * @Last Modified by: Hendranus Vermeulen
 * @Last Modified time: 2020-01-27 10:44:03
 * 
 * Class Model maintains the state of the game and emmits events if the model changes
 * 
 */

import { EventEmitter } from 'events';
import { IModel } from './IModel.js';
import { PGFEvent } from '../events/PGFEvent.js';
import { GameData } from './GameData.js';

class Model extends IModel(EventEmitter) {
    constructor() {
        super();
        //GameData object to manage the game state
        this.data = new GameData();
    }
    /**
     * Set the key for the model's data
     * @param	key
     */
    setKey(key) {
        this.data.key = key;
        console.log(`Model sets key: ${this.data.key}`);
        this.emit(PGFEvent.CHANGE, { key : key });
    }
    /**
     * Get the key from the model's data
     * @return
     */
    getKey() {
        return this.data.key;
    }
    /**
     * Set the State for the model's data
     * @param	state
     */
    setState(state) {
        this.data.currentState = state;
        console.log(`Model sets currentState: ${this.data.currentState}`);
        this.emit(PGFEvent.CHANGE, { currentState : this.data.currentState });
    }
    /**
     * Get the State from the model's data
     * @return
     */
    getState() {
        return this.data.currentState;
    }
}
export { Model }