/*
 * @Author: Hendranus Vermeulen 
 * @Date: 2020-01-25 19:38:13 
 * @Last Modified by: Hendranus Vermeulen
 * @Last Modified time: 2020-01-27 09:25:16
 * 
 * Interface IModel for Model classes to implement
 * 
 */

const IModel = superclass => class extends superclass {
    constructor() {
        super();
        // Check Interface contract is fullfilled
        if ( this.setKey == null || 
             this.getKey == null ||
             this.setState == null ||
             this.getState == null ) 
        {
            // Error Type 3. The child has not implemented this method.
            throw new TypeError("IModel: Fullfill contract, implement all methods.");
        }
    }
};
export { IModel }