/*
 * @Author: Hendranus Vermeulen 
 * @Date: 2020-01-26 10:40:53 
 * @Last Modified by: Hendranus Vermeulen
 * @Last Modified time: 2020-01-27 10:40:07
 * 
 * Class GameData is a object that is instatiated by the model to manage the game state
 * 
 */

import { GameStates } from './GameStates.js';

 class GameData extends Object {
    constructor() {
        super();
        this.reset();
    }
    /**
     * Getter method for data object
     */
    get data() {
        return this;
    }
    /**
     * Reset data to defaults
     */
    reset(){
        this.currentState = GameStates.NOT_LOADED;
        this.key = -1;
        this.percentLoaded = 0;
    }
}
export { GameData }