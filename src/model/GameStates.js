/*
 * @Author: Hendranus Vermeulen 
 * @Date: 2020-01-26 17:37:53 
 * @Last Modified by: Hendranus Vermeulen
 * @Last Modified time: 2020-01-27 10:36:40
 * 
 * Class GameStates is a simple class to use static const for states
 * 
 */
const NOT_LOADED = 'NOT_LOADED';
const LOADED = 'LOADED';

class GameStates {
    static get NOT_LOADED() {
        return NOT_LOADED;
    }
    static get LOADED() {
        return LOADED;
    }
}
export { GameStates }