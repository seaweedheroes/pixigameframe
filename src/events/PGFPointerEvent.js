/*
 * @Author: Hendranus Vermeulen 
 * @Date: 2020-01-25 19:39:13 
 * @Last Modified by: Hendranus Vermeulen
 * @Last Modified time: 2020-01-27 10:29:32
 * 
 * Class PGFPointerEvent is a simple class to use static const for event types
 * I NEED TO INVESTGATE HOW TO BETTER INTEGRATE PIXI POINTER EVENTS AND NATIVE JS EVENTS
 * 
 */
const POINTER_DOWN = 'pointerdown';
const POINTER_UP = 'pointerup';

class PGFPointerEvent {
    static get POINTER_DOWN() {
        return POINTER_DOWN;
    }
    static get POINTER_UP() {
        return POINTER_UP;
    }
}
export { PGFPointerEvent }