/*
 * @Author: Hendranus Vermeulen 
 * @Date: 2020-01-25 19:38:22 
 * @Last Modified by: Hendranus Vermeulen
 * @Last Modified time: 2020-01-27 10:28:06
 * 
 * Class PGFEvent is a simple class to use static const for event types
 * I NEED TO INVESTGATE HOW TO BETTER INTEGRATE PIXI POINTER EVENTS AND NATIVE JS EVENTS
 * 
 */
const CHANGE = 'CHANGE';

class PGFEvent {
    static get CHANGE() {
        return CHANGE;
    }
}
export { PGFEvent }