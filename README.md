## PixiGameFrame

This is a start of a framework for developing PixiJS games and applications using npm, webpack and JavaScript. Having a solid framework based on good OOP greatly facilitates development, maintenance and collaboration.

PixiJS (https://www.pixijs.com/), the fastest WebGL JavaScript engine. PixiJS is great renderer but lacks a solid framework for application and game design. I started the development of the PixiGameFramework based on the classical Model View Controller (MVC) design pattern. The MVC design pattern very useful for developing complex applications. Games are complex there is a lot to manage. Just thinking about displaying visual elements. How do you manage the visual elements (screens, layers, animated sprites, sprites, text, etc.)? MVC offers a flexible yet robust solution. 

The Model contains the logic and data for managing the state of your game. The Model is the subject in the Observer pattern. It registers listener methods to receive event notifications. The View presents the state of the application to the user (usually visually). It can also have UI that the user interacts with. It registers with the model to receive update events and delegates to the controller to handle user input. There can be multiple views and they can be nested to create complex hierarchical Views that update in a cascade. To achieve this we use a Composite design pattern. The Controller handles the user input delegated from Views and changes the state of the application (Model) which in turn updates the View. The relationship between the Controller and View is a Strategy pattern.

Game development is highly iterative and things change fast. With a solid MVC implementation, you can change your game by adding and removing views and controllers to add and change functionality and because M-V-C components are encapsulated and loosely coupled it is robust, flexible and maintainable. The Model is King in MVC! This is very useful in games where the state of the game is dictated by an online service usually described as JSON. This JSON can be parsed as an object to update or replace an object in the Model. This then cascades through the views representing the appropriate game state to the user.

I only had time to do a very basic MVC. Currently, the framework only updates the web console from cascading views when the user clicks on the red button. The framework enforces method contracts using abstract classes and interfaces. JavaScript has reserved the 'Interface' keyword but has not yet implemented it. I implemented my own ‘interfaces’ using class composition to throw errors when the framework is not used as intended (see IModel.js for example).

#### Setup

Install dependencies:
```
npm i
```

Run on http://localhost:3000:
```
npm start
```

#### Building

Development build:
```
npm run build-dev
```

Production build:
```
npm run build-prod
```